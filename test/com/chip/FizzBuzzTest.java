package com.chip;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FizzBuzzTest {
    @Test
    public void testDefaultCase() {
        assertEquals("1", new FizzBuzz().run(1));
        assertEquals("2", new FizzBuzz().run(2));
    }

    @Test
    public void testMultipleOfThree() {
        assertEquals("fizz", new FizzBuzz().run(3));
        assertEquals("fizz", new FizzBuzz().run(6));
    }

    @Test
    public void testMultipleOfFive() {
        assertEquals("buzz", new FizzBuzz().run(5));
        assertEquals("buzz", new FizzBuzz().run(10));
    }

    @Test
    public void testMultipleOfThreeAndFive() {
        assertEquals("fizzbuzz", new FizzBuzz().run(15));
        assertEquals("fizzbuzz", new FizzBuzz().run(30));
    }
}
