package com.chip;

class FizzBuzz {
    String run(int number) {
        String result = "";
        if(isNotaMultipleOfThreeOrFive(number)) {
            result = String.valueOf(number);
        }
        if(isaMultipleOfThree(number)) {
            result = "fizz";
        }
        if(isaMultipleOfFive(number)) {
            result += "buzz";
        }

        return result;
    }

    private boolean isNotaMultipleOfThreeOrFive(int number) {
        return !isaMultipleOfFive(number) && !isaMultipleOfThree(number);
    }

    private boolean isaMultipleOfFive(int number) {
        return number % 5 == 0;
    }

    private boolean isaMultipleOfThree(int number) {
        return number % 3 == 0;
    }
}
